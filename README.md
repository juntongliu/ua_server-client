
This directory contains a simple UA (open62541) server and client that implement several UA variableNodes for a pseudo Cfhvacms device for testing. Server and client commununicate through loop back interface (127.0.0.1). When started, the client periodically qurrey the server for the VariableNodes that it is interested and print out the corresponding values on the terminal window.

To build and run the server and client:

     $ git clone https://gitlab.esss.lu.se/juntongliu/ua_server-client.git
     $ cd ua_server-client
     $ make                 # this will build both server and client
     $ make run             # this will run the server


Open another shell window and run the client:

     $ cd /path/to/ua_server-client
     $ ./client




Following is an example client output:

---------------------------------------------------------------------------------------------------------------


Timestamp from UA server              :  6-6-2022 11:40:46.433

Cfhvacums Device Manufacture name     :  ESS Inkind Partner 111

Cfhvacums Device Serial Number        :  VAC12032_3562EU_32

Cfhvacums Device Integer Parameter  1 :  222

Cfhvacums Device Integer Parameter  2 :  888

Cfhvacums Device Floatpnt Parameter 1 :  55.081061

Cfhvacums Device Integer Parameter  3 :  780



---------------------------------------------------------------------------------------------------------------
