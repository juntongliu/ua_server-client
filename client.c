/**
 *
 * This simple UA client make a connection to a UA server through the loop back interface (127.0.0.1),
 * and querry the UA server continuosly for those variableNodes defined in this client. To make it simple,
 * only several UA variablesNodes have been implemented both in the server and this client.
 * 
 *
 *                                                                         Juntong Liu 
 *                                                                                      2022-06-02       
 */

#include "open62541.h"

#include <signal.h>
#include <string.h>

#define DEBUG
#undef DEBUG
#ifdef DEBUG
#define dprintf(format, args...) printf(format, ##args)
#define DBGPRINT(fmt, ...)                            \
    do                                                \
    {                                                 \
        fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                __LINE__, __func__, __VA_ARGS__);     \
    } while (0)
#else
#define dprintf(format, args...)
#define DBGPRINT(fmt, ...)
#endif

#ifdef _WIN32
# include <windows.h>
# define UA_sleep_ms(X) Sleep(X)
#else
# include <unistd.h>
# define UA_sleep_ms(X) usleep(X * 1000)
#endif

#define WITH_TIMESTAMP
#undef  WITH_TIMESTAMP

UA_Boolean running = true;

static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
    running = false;
}

// Check the varian varialbe type (for debug only)
static void 
check_variant_value(UA_Variant value, int flag)
{
    if(flag == 1)
    {
        printf("In check_variant_value() type function....\n");
        //JT, check if the variant is empty
        if(UA_Variant_isEmpty(&value))
            printf("JT DEBUG: in check_variant_value(): Variant is empty!\n\n");
        else if(UA_Variant_isScalar(&value))
            printf("JT, DEBUG: in check_variant_value(): Variate is Scalar\n\n");      // is a scalar !!!
        
        if(UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_DATETIME]))
            printf("JT DEBUG: in check_variant_value(): Variant has scalar type! and UA_TYPE_DATETIME \n\n");
        
        if(UA_Variant_hasArrayType(&value, &UA_TYPES[UA_TYPES_DATETIME]))
            printf("JT DEBUG: in check_variant_value: Variant is a Array! and UA_TYPE_DATETIME \n\n");
    }
}

static void
print_title(void)
{
    printf("\n\n\n");
    printf("------------------------------------------------------------------------\n\n");

}

int main(void) 
{
    int number_tries;

    signal(SIGINT, stopHandler);        /* catches ctrl-c */

    UA_Client *client = UA_Client_new();
    UA_ClientConfig_setDefault(UA_Client_getConfig(client));
    UA_StatusCode retval = UA_Client_connect(client, "opc.tcp://localhost:4840");
    if(retval != UA_STATUSCODE_GOOD) {
        UA_Client_delete(client);
        return (int)retval;
    }

    /* Read the value attribute of the node. UA_Client_readValueAttribute is a
     * wrapper for the raw read service available as UA_Client_Service_read. */
    UA_Variant value;                /* Variants can hold scalar values and arrays of any type */
    UA_Variant_init(&value);

    /* Endless loop reading the server time */
    while (running) 
    {
        system("clear");  // clear the screen 
        print_title();
        printf("\n");
        /* NodeId of the variable holding the current time */
        //const 
        UA_NodeId nodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_SERVER_SERVERSTATUS_CURRENTTIME);

        retval = UA_Client_readValueAttribute(client, nodeId, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
	    number_tries++;
	    if(number_tries > 20)
	    {
		printf("Connection failed!\n");
		break;
	    }
            //UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_CLIENT, "Connection was closed. Reconnecting ...");
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }

        if (retval == UA_STATUSCODE_GOOD && UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_DATETIME])) 
        {
            UA_DateTime raw_date = *(UA_DateTime *) value.data;
            UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We got system timestamp from UA server:");
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Timestamp from UA server: %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
#else
            printf("Timestamp from UA server             :  %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
#endif
        }

// 1. ---------------------------------------------------------n
        UA_Variant_init(&value);
        UA_NodeId nodeId1 = UA_NODEID_STRING(1, "1, current-time-value-callback");
        retval = UA_Client_readValueAttribute(client, nodeId1, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }       

        check_variant_value(value, 0);     
        
        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {
            UA_DateTime raw_date = *(UA_DateTime *) value.data;
            UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We got current-time-value-callback:");
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Client connected to server since: %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
        }        
    
// 2. ---------------------------------------------------------n

        UA_Variant_init(&value);
        //const 
        UA_NodeId nodeId3 = UA_NODEID_STRING(1, "dev.manufacture_name");
        retval = UA_Client_readValueAttribute(client, nodeId3, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }
        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {
            UA_String *strp = (UA_String *)value.data;
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Manufacture name: %s\n", strp->data);
#else
            printf("\nCfhvacms Device Manufacture name     :  %s\n", strp->data);
#endif
        }
        
        //UA_String *strp = (UA_String *)value.data;
        //printf("The string length: %zu; String is: %s\n", strp->length, strp->data);

        check_variant_value(value, 0);

// --------------------------------------------------------
         UA_Variant_init(&value);
        //const 
        UA_NodeId devSerialNodeId = UA_NODEID_STRING(1, "dev.serial_number");
        retval = UA_Client_readValueAttribute(client, devSerialNodeId, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }
        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {
            UA_String *strp = (UA_String *)value.data;
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Serial Number: %s\n", strp->data);
#else
            printf("\nCfhvacms Device Serial Number        :  %s\n", strp->data);
#endif
        }       

// 3 ----------------------------------------------------------

        UA_Variant_init(&value);
        //const 
        UA_NodeId nodeId4 = UA_NODEID_STRING(1, "ineger.parameter_1");
        retval = UA_Client_readValueAttribute(client, nodeId4, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }
        dprintf("The Variant is 11111: %d\n", *(unsigned int *)value.data);
        check_variant_value(value, 0);
        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {
            //UA_DateTime raw_date = *(UA_DateTime *) value.data;
            
            //UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
            //UA_String string_date = UA_DateTime_toString(raw_date);
            //UA_LOG_INFO(logger, UA_LOGCATEGORY_CLIENT, "string date is: %.*s", (int) string_date.length, string_date.data);
            dprintf("JT DEBUG::: 444 =====> here\n");
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We got UA device operating parameter-1 :");
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Integer Parameter  1: %d\n", *(unsigned int*)(value.data));
#else
            printf("\nCfhvacms Device Integer Parameter  1 :  %d\n", *(unsigned int*)(value.data));
#endif
        }

// 4  ----------------------------------------------------------

        /* NodeId of the variable holding the current time */
        UA_Variant_init(&value);
        //const 
        UA_NodeId nodeId2 = UA_NODEID_STRING(1, "ineger.parameter_2");
        retval = UA_Client_readValueAttribute(client, nodeId2, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }

        check_variant_value(value, 0);

        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {

            dprintf("The Variant is 22222: %d\n", *(unsigned int *)value.data);
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We got client.connected_since:");
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "This client connected to server since: %u-%u-%u %u:%u:%u.%03u\n",
            //        dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Integer Parameter 2: %d\n", (unsigned int)(value.data));
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Integer Parameter  2: %d\n", *(unsigned int *)(value.data));
#else   
            printf("\nCfhvacms Device Integer Parameter  2 :  %d\n", *(unsigned int *)(value.data));
#endif
        }
        // JT, we check if there is a timestamp with the variable
        if (retval == UA_STATUSCODE_GOOD && UA_Variant_hasScalarType(&value, &UA_TYPES[UA_TYPES_DATETIME]))
        {
            UA_DateTime raw_date = *(UA_DateTime *) value.data;
            UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "This client connected to server since: %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
        } 
#if 0
        /* JT test to see how to use UA_WriteValue structure  
           UA_WriteValue {
             struct ua_nodeid 	node_id;  // NodeId of the node that contains the attributes.
             uint32_t 	attribute_id;     // Id of the attribute. More... 
             struct ua_string 	index_range; // This parameter is used to identify a single element of an array, or a single range of indexes for arrays. More...
             struct ua_datavalue 	value; // The node’s attribute value. More...
            }
           UA_DataValue {
               struct ua_variant 	value;
               ua_datetime 	server_timestamp;
               ua_datetime 	source_timestamp;
           }
        */
        UA_WriteValue *wv = (UA_WriteValue *)(value.data);
        printf("JT DEBUG UA_WriteValue: wv->value.value = %d\n; ", wv->value.value);
        if(wv->value.hasSourceTimestamp)
        {
            UA_DateTime raw_date = *(UA_DateTime *) wv->value.data;
            UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "This client connected to server since: %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
        }
#endif 

// 5  ----------------------------------------------------- dobule parameter with call back function

        UA_Variant_init(&value);
        UA_NodeId nodeId5 = UA_NODEID_STRING(1, "double.parameter_1");
        retval = UA_Client_readValueAttribute(client, nodeId5, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }
        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We get a double parameter from server :");
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Floatpnt Parameter 1: %f\n", *(UA_Double *)value.data);
#else
            printf("\nCfhvacms Device Floatpnt Parameter 1 :  %f\n", *(UA_Double *)value.data);
#endif
        }

// 6. ---------------------------------------------------- integer parameter with call back function

        UA_Variant_init(&value);
        UA_NodeId nodeId7 = UA_NODEID_STRING(1, "integer.parameter_3");
        retval = UA_Client_readValueAttribute(client, nodeId7, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED) 
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }

        check_variant_value(value, 0);

        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value)) 
        {
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We get a double parameter from server :");
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Integer Parameter 3: %d\n", *(UA_Int32 *)value.data);
#else
            printf("\nCfhvacms Device Integer Parameter  3 :  %d\n", *(UA_Int32 *)value.data);
#endif
        }

// 7. --------------------------------- test the strange nodeId
        UA_Variant_init(&value);
        UA_NodeId nodeId6 = UA_NODEID_STRING(1, "0:\\\\APL\\1\\P\\J1_U01_M\\10-JT");
        retval = UA_Client_readValueAttribute(client, nodeId6, &value);
        if (retval == UA_STATUSCODE_BADCONNECTIONCLOSED)
        {
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Connection was closed. Reconnecting...\n");
            printf("Connection was closed. Reconnecting ...\n");
            continue;
        }

        check_variant_value(value, 0);

        if (retval == UA_STATUSCODE_GOOD && UA_Variant_isScalar(&value))
        {
            //UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "We get a double parameter from server :");
#ifdef WITH_TIMESTAMP
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Cfhvacms Device Integer Parameter 3: %d\n", *(UA_Int32 *)value.data);
#else
            printf("\n The strange nodeId(0:\\\\APL\\1\\P\\J1_U01_M\\10) nodeSet value :  %d\n", *(UA_Int32 *)value.data);
#endif
        }



        print_title();

        //UA_sleep_ms(1000);
        UA_sleep_ms(1500);
    };

    /* Clean up */
    // JT, no exit any more
    //UA_Variant_deleteMembers(&value);
    //UA_Client_delete(client); /* Disconnects the client internally */
    UA_Variant_clear(&value);
    UA_Client_delete(client); /* Disconnects the client internally */

    return UA_STATUSCODE_GOOD;
}
