/* 
 * This is an simple UA server which simulates a server serving a device system.  The server 
 * create some variableNodes. Each variableNode carry a parameter of the device. 
 * 
 * An UA simple client has also been developed which will periodically querry the UA server
 * to retrieve those variables and display the corresponding values.
 *
 * 
 * 
 *                                                    Juntong Liu 
 *                                                                  2022-06-03 
 * * * * *  */


/* JT, we do not use this headers, instead
#include <open62541/plugin/log_stdout.h>
#include <open62541/server.h>
#include <open62541/server_config_default.h>
we use this one */

#include "open62541.h"

#include <signal.h>
#include <stdlib.h>
#include <time.h>

#define DEBUG
#undef DEBUG
#ifdef DEBUG
#define dprintf(format, args...) printf(format, ##args)
#define DBGPRINT(fmt, ...)                            \
    do                                                \
    {                                                 \
        fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                __LINE__, __func__, __VA_ARGS__);     \
    } while (0)
#else
#define dprintf(format, args...)
#define DBGPRINT(fmt, ...)
#endif

// 1. -----------------------------------------------------------------

/* Function to update a Variant of scalar variable which contain DateTime type value */
static void updateCurrentTime(UA_Server *server) 
{
    UA_DateTime now = UA_DateTime_now();
    UA_Variant value;
    UA_Variant_setScalar(&value, &now, &UA_TYPES[UA_TYPES_DATETIME]);
    UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-value-callback");
    UA_Server_writeValue(server, currentNodeId, value);
}

/* Add a UA variableNode to the UA server. This variableNode has call-back functions associated with it.
* beforeReadTime() and afterWriteTime(). For now, only the beforeReadTime() call back update the variable, 
* the afterWriteTime() call-back does nothing, actually we can add something
* to let it do something to the variable when this variableNode is written .....  */
static void addCurrentTimeVariable(UA_Server *server) 
{
    UA_DateTime now = 0;
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    attr.displayName = UA_LOCALIZEDTEXT("en-US", "Current time - value callback");
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    UA_Variant_setScalar(&attr.value, &now, &UA_TYPES[UA_TYPES_DATETIME]);

    UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-value-callback");
    UA_QualifiedName currentName = UA_QUALIFIEDNAME(1, "current-time-value-callback");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_NodeId variableTypeNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE);
    UA_Server_addVariableNode(server, currentNodeId, parentNodeId,
                              parentReferenceNodeId, currentName,
                              variableTypeNodeId, attr, NULL, NULL);

    updateCurrentTime(server);
}

/**
 * Variable Value Callback
 * ^^^^^^^^^^^^^^^^^^^^^^^
 * When a value changes continuously, such as the system time, updating the
 * value in a tight loop would take up a lot of resources. Value callbacks allow
 * to synchronize a variable value with an external representation. They attach
 * callbacks to the variable that are executed before every read and after every
 * write operation. */
static void beforeReadTime(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeid, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data) {
    updateCurrentTime(server);
}

static void afterWriteTime(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeId, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "The variable was updated");
}

// Attach the callback functions to the variableNode
static void addValueCallbackToCurrentTimeVariable(UA_Server *server) {
    UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-value-callback");
    UA_ValueCallback callback ;
    callback.onRead = beforeReadTime;
    callback.onWrite = afterWriteTime;
    UA_Server_setVariableNode_valueCallback(server, currentNodeId, callback);
}

/**
 * Variable Data Sources
 * ^^^^^^^^^^^^^^^^^^^^^
 * With value callbacks, the value is still stored in the variable node.
 * So-called data sources go one step further. The server redirects every read
 * and write request to a callback function. Upon reading, the callback provides
 * a copy of the current value. Internally, the data source needs to implement
 * its own memory management. */

static UA_StatusCode 
readCurrentTime(UA_Server *server,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_NodeId *nodeId, void *nodeContext,
                UA_Boolean sourceTimeStamp, const UA_NumericRange *range,
                UA_DataValue *dataValue) {
    UA_DateTime now = UA_DateTime_now();
    UA_Variant_setScalarCopy(&dataValue->value, &now, &UA_TYPES[UA_TYPES_DATETIME]);
    dataValue->hasValue = true;
    return UA_STATUSCODE_GOOD;
}

static UA_StatusCode 
writeCurrentTime(UA_Server *server,
                 const UA_NodeId *sessionId, void *sessionContext,
                 const UA_NodeId *nodeId, void *nodeContext,
                 const UA_NumericRange *range, const UA_DataValue *data) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "Changing the system time is not implemented");
    return UA_STATUSCODE_BADINTERNALERROR;
}

static void 
addCurrentTimeDataSourceVariable(UA_Server *server) 
{
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    attr.displayName = UA_LOCALIZEDTEXT("en-US", "Current time - data source");
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-datasource");
    UA_QualifiedName currentName = UA_QUALIFIEDNAME(1, "current-time-datasource");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_NodeId variableTypeNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE);

    UA_DataSource timeDataSource;
    timeDataSource.read = readCurrentTime;
    timeDataSource.write = writeCurrentTime;
    UA_Server_addDataSourceVariableNode(server, currentNodeId, parentNodeId,
                                        parentReferenceNodeId, currentName,
                                        variableTypeNodeId, attr,
                                        timeDataSource, NULL, NULL);
}

static UA_DataValue *externalValue;

static void addCurrentTimeExternalDataSource(UA_Server *server) {
    UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-external-source");

    UA_ValueBackend valueBackend;
    valueBackend.backendType = UA_VALUEBACKENDTYPE_EXTERNAL;
    valueBackend.backend.external.value = &externalValue;

    UA_Server_setVariableNode_valueBackend(server, currentNodeId, valueBackend);
}

// 0 -------------------------------------------

static void
addDeviceSerialNumber(UA_Server *server)
{
    UA_String tmpstr = UA_STRING("VAC2022032158S1");
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Variant_setScalarCopy(&attr.value, &tmpstr, &UA_TYPES[UA_TYPES_STRING]);
    attr.description = UA_LOCALIZEDTEXT("en-US", "CFHVACUMS Device Serial Number");
    attr.displayName = UA_LOCALIZEDTEXT("en-US", "CFHVACUMS Device Serial Number");
    attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable to the server */
    UA_NodeId  myStringNodeId = UA_NODEID_STRING(1, "dev.serial_number");
    UA_QualifiedName myStringName = UA_QUALIFIEDNAME(1, "dev.serial_number");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myStringNodeId, parentNodeId,
                              parentReferenceNodeId, myStringName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
}

static void
writeDevSerialNumber(UA_Server *server, char *nstr)
{
    UA_NodeId devSeId = UA_NODEID_STRING(1, "dev.serial_number");
    UA_Variant value;
    UA_Variant_init(&value);
    UA_String tmp_str = UA_STRING(nstr);
    UA_Variant_setScalarCopy(&value, &tmp_str, &UA_TYPES[UA_TYPES_STRING]);    // JT copy an UA_String to variant
    UA_Server_writeValue(server, devSeId, value);
}

// 1 -------------------------------------------

static void
add_dev_manufactureName(UA_Server *server)
{
    UA_String tmpstr = UA_STRING("Device manufacture");
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Variant_setScalarCopy(&attr.value, &tmpstr, &UA_TYPES[UA_TYPES_STRING]);
    //printf("ddddddd = %s\n", UA_String_data(attr.value.data));
    attr.description = UA_LOCALIZEDTEXT("en-US", "CFHVACUMS Device Manufecture Name");
    attr.displayName = UA_LOCALIZEDTEXT("en-US", "CFHVACUMS Device Manufecture Name");
    attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    dprintf("=====>String Value: attr.value.data=%s; length = %zu\n", tmpstr.data, tmpstr.length ); // attr.value.data);

    /* Add the variable to the server */
    UA_NodeId  myStringNodeId = UA_NODEID_STRING(1, "dev.manufacture_name");
    UA_QualifiedName myStringName = UA_QUALIFIEDNAME(1, "dev.manufacture_name");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myStringNodeId, parentNodeId,
                              parentReferenceNodeId, myStringName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
}

static void
write_dev_manufactureName(UA_Server *server, char *nstr)
{
    UA_NodeId dnId = UA_NODEID_STRING(1, "dev.manufacture_name");
    UA_Variant value;
    UA_Variant_init(&value);
    UA_String tmp_str = UA_STRING(nstr);
    UA_Variant_setScalarCopy(&value, &tmp_str, &UA_TYPES[UA_TYPES_STRING]);    // JT copy an UA_String to variant
    UA_Server_writeValue(server, dnId, value);
}

// 2. -------------------------------------------

// Create and add an int32 variableNode to the UA server.
static void 
addVariable_1(UA_Server *server) {
    /* Define the attribute of the myInteger variable node */
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Int32 myInteger = 0;
    UA_Variant_setScalar(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    attr.description = UA_LOCALIZEDTEXT("en-US","Cfhvacms Device Ingeger Parameter 1");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","Cfhvacms Device Ingeger Parameter 1");
    attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable node to the information model (server) */
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "ineger.parameter_1");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME(1, "ineger.parameter_1");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
 
}

// Manually set a new value to the int32 variableNode 
static void
writeVariable_1(UA_Server *server, UA_Int32 myInteger ) 
{
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "ineger.parameter_1");

    UA_Variant value;
    UA_Variant_init(&value);
    UA_Variant_setScalar(&value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);

#if 0    
    /* JT, this will write a value with server side timestamp in the strucure and the client side can get both
      variable value and server side time step. Client get the value are same (see below integer.parameter_2). 
      But how to the timestamp on client side? The tricky is: the UA_String is a structure! it has 2 members,
      length and data. So, if we got a Variant, the data field is a "UA_String" structure. We need to cast it
      into a "UA_String *" pointer to access the number !!! */
    /* Write a different integer value */
    UA_VariableAttributes Attr;
    UA_Variant_init(&Attr.value);
    UA_Variant_setScalar(&Attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
  
    // Use a more detailed write function than UA_Server_writeValue
    UA_WriteValue wv;
    UA_WriteValue_init(&wv);
    wv.nodeId = myIntegerNodeId;
    wv.attributeId = UA_ATTRIBUTEID_VALUE;
    wv.value.hasStatus = false;
    wv.value.value = Attr.value;
    wv.value.hasValue = true;
    UA_DateTime currentTime = UA_DateTime_now();
    wv.value.hasSourceTimestamp = true;
    wv.value.sourceTimestamp = currentTime - 1800 * UA_DATETIME_SEC;
    //printf("JT DEBUG:===>: currentTime = %lld\n", currentTime);
 
    UA_Server_writeDataValue(server, myIntegerNodeId, wv.value);

#endif

    UA_Server_writeValue(server, myIntegerNodeId, value);

}


// 3. -----------------------------------------------

static void
addVariable_3(UA_Server *server) {
    /* Define the attribute of the myInteger variable node */
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Int32 myInteger = 0;
    UA_Variant_setScalar(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    attr.description = UA_LOCALIZEDTEXT("en-US","Cfhvacms Device Integer Parameter 2");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","Cfhvacms Device Integer Parameter 2");
    attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable node to the information model */
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "ineger.parameter_2");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME(1, "ineger.parameter_2");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);

}


/**
 * Now we change the value with the write service. This uses the same service
 * implementation that can also be reached over the network by an OPC UA client.
 */

static void
writeVariable_3(UA_Server *server, UA_Int32 myInteger ) {
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "ineger.parameter_2");

    /* Write a different integer value */
    UA_VariableAttributes Attr;
    UA_Variant_init(&Attr.value);
    UA_Variant_setScalar(&Attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);

    // Use a more detailed write function than UA_Server_writeValue
    UA_WriteValue wv;
    UA_WriteValue_init(&wv);
    wv.nodeId = myIntegerNodeId;
    wv.attributeId = UA_ATTRIBUTEID_VALUE;
    wv.value.hasStatus = false;
    wv.value.value = Attr.value;
    wv.value.hasValue = true;

    UA_DateTime currentTime = UA_DateTime_now();
    wv.value.hasSourceTimestamp = true;
    wv.value.sourceTimestamp = currentTime ; // - 1800 * UA_DATETIME_SEC;
    dprintf("JT DEBUG: 1. ===>: currentTime = %lld\n", currentTime);

    UA_Server_writeDataValue(server, myIntegerNodeId, wv.value );

}

// 4. ----------------------------------------- UA variableNode of bouble type with call back and random number

/* Add another double variableNode to UA server. And create and associate a callback function
*  to the variableNode. The call-back function will be called every 500ms to check the "data source"
*  and update the value of this variableNode accordingly. Since we do not have a real device here,
*  I just create a function to generate a random number each time the call-back function is called
*  to simulate a data source. 
*/
static void
addVariable_4(UA_Server *server)
{
    // add a variable node to the adresspace/server
    //UA_Int32 myInteger = 42;
    //UA_Variant_setScalarCopy(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Double myDouble = 0.111;                                  // give a initial value
    UA_Variant_setScalarCopy(&attr.value, &myDouble, &UA_TYPES[UA_TYPES_DOUBLE]);
    attr.description = UA_LOCALIZEDTEXT_ALLOC("en-US","Cfhvacms Floatpoint Parameter 1");
    attr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US","Cfhvacms Floatpoint Parameter 1");
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING_ALLOC(1, "double.parameter_1");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME_ALLOC(1, "double.parameter_1");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NULL, attr, NULL, NULL);
}

/* It seems that this function should be called only one time. If we created a loop to call this function
* repeatedly, we can do it, but, it may consume more resource.  And, there is a post in a forum say that, the
* UA only call this onece even in a loop!  https://github.com/open62541/open62541/issues/2749
* According to a answer in this forum, one usually create a call back function and add it to the variableNode
* to do repeatedly tasks, like the following:
*/
static void
writeVariable_4(UA_Server *server, UA_Double myDouble)
{
    UA_NodeId doubleNodeId = UA_NODEID_STRING(1, "double.parameter_1");
    // UA_VariableAttributes attr;
    // UA_Variant_init(&attr.value);
    // UA_Variant_setScalar(&attr.value, &myDouble, &UA_TYPES[UA_TYPES_DOUBLE]);
    UA_Variant value;
    UA_Variant_init(&value);
    UA_Variant_setScalar(&value, &myDouble, &UA_TYPES[UA_TYPES_DOUBLE]);

    //UA_Server_writeDataValue(server, doubleNodeId, attr.value);   // this need UA_WriteValue rv; UA_WriteValue_init(&rv), ....
    UA_Server_writeValue(server, doubleNodeId, value);
}

double myRandom_DoubleNumber()
{
    return (double)rand() / ((double)RAND_MAX + 1) ;
}

/* JT, this variable is a global variable. This means that this should be associated to a reading from a real 
* device and use it to update the corresponding UA variableNode in a call-back function associated with the UA variableNode.
* Since there is no this kind of reading (no device), in the following call back function, I use a function to generate a 
* random number + a base to make it like some kind of simulation - alive values */
UA_Variant valueVariant;                                
UA_Boolean actVal;
// this function is called every 500 ms after registering
static void writeCallback_test(UA_Server *server, void *data) 
{
    UA_NodeId node_id = UA_NODEID_STRING(1, "double.parameter_1");
    //UA_NodeId node_id = UA_NODEID_NUMERIC(1, 52510);
    actVal = false;
    UA_Variant_setScalar(&valueVariant, &actVal, &UA_TYPES[UA_TYPES_BOOLEAN]);
    
    UA_Variant myData;
    UA_Variant_init(&myData);
    UA_Double tdouble = myRandom_DoubleNumber() + 55;   // make the input changing around 55
    //UA_Variant_setScalar(&myData, (UA_Variant *)data, &UA_TYPES[UA_TYPES_BOOLEAN]);
    //UA_Variant_setScalar(&myData, (UA_Variant *)data, &UA_TYPES[UA_TYPES_DOUBLE]);
    UA_Variant_setScalar(&myData, &tdouble, &UA_TYPES[UA_TYPES_DOUBLE]);
    dprintf("JT DEBUT::: ===> The writeCallback_test is called! myData.data = %f\n", *((UA_Double *)myData.data));

    //UA_Server_writeValue(server, node_id , valueVariant);  // original
    UA_Server_writeValue(server, node_id , myData);
}

// 5. ----------------------------------------- UA variableNode int32 type with call back and random number
static void
addVariable_Integer3(UA_Server *server)
{
    // add a variable node to the adresspace/server
    UA_Int32 myInteger = 42;
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Variant_setScalarCopy(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    //UA_Double myDouble = 0.111;                                  // give a initial value
    //UA_Variant_setScalarCopy(&attr.value, &myDouble, &UA_TYPES[UA_TYPES_DOUBLE]);
    attr.description = UA_LOCALIZEDTEXT_ALLOC("en-US","Cfhvacms Integer Parameter 3");
    attr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US","Cfhvacms Integer Parameter 3");
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING_ALLOC(1, "integer.parameter_3");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME_ALLOC(1, "integer.parameter_3");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NULL, attr, NULL, NULL);
}

static void
writeVariable_Integer3(UA_Server *server, UA_Int32 myInteger)
{
    UA_NodeId integerNodeId = UA_NODEID_STRING(1, "integer.parameter_3");
    // UA_VariableAttributes attr;
    // UA_Variant_init(&attr.value);
    // UA_Variant_setScalar(&attr.value, &myDouble, &UA_TYPES[UA_TYPES_DOUBLE]);
    UA_Variant value;
    UA_Variant_init(&value);
    UA_Variant_setScalar(&value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);

    //UA_Server_writeDataValue(server, doubleNodeId, attr.value);   // this need UA_WriteValue rv; UA_WriteValue_init(&rv), ....
    UA_Server_writeValue(server, integerNodeId, value);
}

int myRandom_IntNumber()
{
    return 10*((double)rand() / ((double)RAND_MAX + 1)) ;
}

// this function is called every 1000 ms after registering
static void writeCallback_IntParameter_3(UA_Server *server, void *data) 
{
    UA_NodeId node_id = UA_NODEID_STRING(1, "integer.parameter_3");
    //UA_NodeId node_id = UA_NODEID_NUMERIC(1, 52510);
    actVal = false;
    UA_Variant_setScalar(&valueVariant, &actVal, &UA_TYPES[UA_TYPES_BOOLEAN]);
    
    UA_Variant myData;
    UA_Variant_init(&myData);
    UA_Int32 tInt32 = myRandom_IntNumber() + 777;   // make the input changing around 55
    //UA_Variant_setScalar(&myData, (UA_Variant *)data, &UA_TYPES[UA_TYPES_BOOLEAN]);
    //UA_Variant_setScalar(&myData, (UA_Variant *)data, &UA_TYPES[UA_TYPES_DOUBLE]);
    UA_Variant_setScalar(&myData, &tInt32, &UA_TYPES[UA_TYPES_INT32]);
    dprintf("JT DEBUT::: ===> The writeCallback_test is called! myData.data = %D\n", *((UA_Int32 *)myData.data));

    //UA_Server_writeValue(server, node_id , valueVariant);  // original
    UA_Server_writeValue(server, node_id , myData);
}

// ---- End 5. ---- 
//
// 6. ------------------------------- test the strange string nodeId

static void
addVariable_testStrangeNodeId(UA_Server *server) {
    /* Define the attribute of the myInteger variable node */
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Int32 myInteger = 555555;
    UA_Variant_setScalar(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    attr.description = UA_LOCALIZEDTEXT("en-US","Test the strange string nodeId");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","Test the strange string nodeId");
    attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable node to the information model */
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "0:\\\\APL\\1\\P\\J1_U01_M\\10-JT");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME(1, "ineger.parameter_2");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);

}


// end of 6

/** It follows the main server code, making use of the above definitions. */

static volatile UA_Boolean running = true;
static void stopHandler(int sign) 
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "received ctrl-c");
    running = false;
}

int main(void) {
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    UA_Server *server = UA_Server_new();
    UA_ServerConfig_setDefault(UA_Server_getConfig(server));

    addVariable_4(server);            // Create a static double variableNode with init value 0.111
    writeVariable_4(server, 0.237);   // change the value to 0.237  
    // JT, register a call back function to double.parameter_1 that will be called every 500ms */
    UA_Variant dataValue;
    UA_Variant_init(&dataValue);
    UA_Double myDouble = 0.56;
    UA_Variant_setScalar(&dataValue, &myDouble, &UA_TYPES[UA_TYPES_DOUBLE]);
    UA_Server_addRepeatedCallback(server, writeCallback_test, &dataValue, 500, NULL);  // install call back
    //UA_Server_addRepeatedCallback(server, writeCallback_test, &dataValue, 500, NULL);

    addVariable_Integer3(server);
    writeVariable_Integer3(server, 777777);
    // register a call back function to the integer.parameter_3
    UA_Variant_init(&dataValue);
    UA_Int32 myInt = 7777;
    UA_Variant_setScalar(&dataValue, &myInt, &UA_TYPES[UA_TYPES_INT32]);
    UA_Server_addRepeatedCallback(server, writeCallback_IntParameter_3, &dataValue, 1000, NULL);  // install call back call every 1000ms

#if 1
    add_dev_manufactureName(server);
    //UA_String value_s = UA_STRING("test-test-test");
    write_dev_manufactureName(server, "ESS Inkind Partner 111");
#endif 

    addVariable_testStrangeNodeId(server);

    addDeviceSerialNumber(server);
    writeDevSerialNumber(server, "VAC12032_3562EU_32");
    
    addVariable_1(server);
    writeVariable_1(server, 222); 

//writeVariable_4(server, 0.732);    // multi-call to this function works

    addVariable_3(server); 
    writeVariable_3(server, 888);

    addCurrentTimeVariable(server);
    addValueCallbackToCurrentTimeVariable(server);
    addCurrentTimeDataSourceVariable(server);

    addCurrentTimeExternalDataSource(server);

    UA_StatusCode retval = UA_Server_run(server, &running);

    UA_Server_delete(server);
    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}
