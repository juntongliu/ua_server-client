
LOCAL_INCLUDE := .
CFLAGS := -Wall -g -std=c99
LDFLAGS := -L. -L./.

all: process_first

process_first:
	gcc $(CFLAGS) open62541.c server.c -o server
	gcc $(CFLAGS) open62541.c client.c -o client

clean:
	@rm server client 

run:
	./server
